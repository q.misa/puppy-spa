export interface IWaitingListEntry {
  id: string;
  arrival: string | number | Date;
  owner: string;
  puppyName: string;
  requestedService: string;
  serviced: boolean;
  prevEntryId?: string;
  nextEntryId?: string;
}
// export interface IWaitingListEntry extends IWaitingListEntries {
//   id: string;
// }

export interface IWaitingListData {
  date: string;
  entries: IWaitingListEntry[];
}
export interface ISearchList {
  searchKey: string;
  value: string | boolean;
}
