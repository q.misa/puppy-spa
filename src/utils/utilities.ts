export const formatErrorMessages = (errors = []) => {
  return typeof errors === 'string' ? errors : errors.join(' , ');
};
