import React from 'react';
import { render, screen } from '@testing-library/react';
import WaitingListSearch from 'components/waitingList/WaitingListSearch';
import { SEARCH, ENTER_SEARCH_VALUE } from 'constants/texts';

describe('WaitingListSearch', () => {
  test('renders WaitingListSearch', () => {
    const search = jest.fn();
    render(<WaitingListSearch search={search} />);

    expect(screen.getByText(SEARCH)).toBeInTheDocument();
    expect(screen.getByPlaceholderText(ENTER_SEARCH_VALUE)).toBeInTheDocument();
  });
});
