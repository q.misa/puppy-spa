import React from 'react';
import { render, screen } from '@testing-library/react';
import HeaderComponent from 'components/general/HeaderComponent';

describe('HeaderComponent', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      value: jest.fn(() => {
        return {
          matches: true,
          addListener: jest.fn(),
          removeListener: jest.fn(),
        };
      }),
    });
  });
  test('renders HeaderComponent', () => {
    render(<HeaderComponent />);
    expect(screen.getByTestId('header')).toBeInTheDocument();
  });
});
