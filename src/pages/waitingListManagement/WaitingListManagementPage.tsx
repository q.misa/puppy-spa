// node modules
import * as React from 'react';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
// components :
import WaitingListComponent from 'components/waitingList/WaitingListComponent';
import WaitingListItemForm from 'components/waitingList/WaitingListItemForm';
import WaitingListActions from 'components/waitingList/WaitingListActions';
import ModalForm from 'components/general/ModalForm';
// actions
import {
  fetchWaitingListSuccess,
  fetchWaitingListError,
  createNewItem,
  deleteItem,
  reorderListData,
  searchListData,
  filterListData,
} from 'actions/waitingListManagementActions';
// apis
import waitingListManagementApis from 'apis/waitingListManagementApis';
// reducers
import { initialState, reducer } from 'reducers/waitingListManagementReducer';
// hooks
import useModal from 'hooks/useModal';
// utils
import { showSuccessNotification } from 'utils/notifications';
// types
import {
  IWaitingListData,
  IWaitingListEntry,
} from 'types/waitingListManagement';
// texts
import { ADD_NEW_ITEM, NEW_RECORD_ADDED_SUCCESSFULLY } from 'constants/texts';
// styles
import 'styles/waitingList.css';

// all the CRUD should handle by real APIs. but here try to implement in front end !
const WaitingListManagementPage: React.FC = () => {
  // ***************************************************** STATES ***********************************************
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const { isShowing, toggle } = useModal();
  const modalRef: any = React.useRef();
  // const id = React.useId();

  // ***************************************************** FUNCTIONS ***********************************************

  const showModal = React.useCallback(() => {
    toggle('new-item');
  }, [toggle]);

  const submitNewItem = React.useCallback(
    (values: IWaitingListEntry) => {
      // @ts-ignore
      values = { ...values, arrival: values['arrival'].format('HH:mm:ss') };
      dispatch(createNewItem(values));
      modalRef.current.setSpin(false);
      toggle('');
      showSuccessNotification(NEW_RECORD_ADDED_SUCCESSFULLY);
    },
    [toggle],
  );

  const handleOnDeleteItem = React.useCallback((selectedItem) => {
    dispatch(deleteItem(selectedItem));
  }, []);

  const reorderList = React.useCallback(() => {
    dispatch(reorderListData());
  }, []);

  const search = React.useCallback(({ searchKey, value }) => {
    if (searchKey && value) {
      dispatch(searchListData({ searchKey, value: value.toLowerCase() }));
    } else {
      fetchData();
    }
  }, []);

  const handleOnChange = React.useCallback((e: CheckboxChangeEvent) => {
    dispatch(
      filterListData({ searchKey: 'serviced', value: e.target.checked }),
    );
  }, []);

  const resetFilters = React.useCallback(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    waitingListManagementApis
      .getWaitingListData({})
      .then((response: IWaitingListData) => {
        dispatch(fetchWaitingListSuccess(response));
      })
      .catch((error) => {
        dispatch(fetchWaitingListError(error));
      });
  };

  // ***************************************************** USE EFFECTS ***********************************************
  React.useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <WaitingListActions
        reorderList={reorderList}
        handleOnChange={handleOnChange}
        search={search}
        showModal={showModal}
        resetFilters={resetFilters}
      />
      <ModalForm
        ref={modalRef}
        initialValues={initialState.waitingListItemFormValues}
        title={ADD_NEW_ITEM}
        size="large"
        width={700}
        visible={isShowing === 'new-item'}
        dataType="waiting-list-form"
        submitForm={submitNewItem}
        onCancel={toggle}
      >
        <WaitingListItemForm mode="create" />
      </ModalForm>
      {state.waitingListData?.entries.length && (
        <WaitingListComponent
          waitingListData={state.waitingListData}
          deleteItem={handleOnDeleteItem}
        />
      )}
    </>
  );
};

export default WaitingListManagementPage;
