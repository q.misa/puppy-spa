import {
  ISearchList,
  IWaitingListData,
  IWaitingListEntry,
} from 'types/waitingListManagement';

export enum WaitingListManagementAction {
  GET_WAITING_LIST_REQUEST = '@@waitingList/GET_WAITING_LIST_REQUEST',
  GET_WAITING_LIST_SUCCESS = '@@waitingList/GET_WAITING_LIST_SUCCESS',
  GET_WAITING_LIST_ERROR = '@@waitingList/GET_WAITING_LIST_ERROR',
  CREATE_NEW_ITEM = '@@waitingList/CREATE_NEW_ITEM',
  DELETE_ITEM = '@@waitingList/DELETE_ITEM',
  REORDER_LIST_DATA = '@@waitingList/REORDER_LIST_DATA',
  SEARCH_LIST_DATA = '@@waitingList/SEARCH_LIST_DATA',
  FILTER_LIST_DATA = '@@waitingList/FILTER_LIST_DATA',
}

export type Action =
  | { type: WaitingListManagementAction.GET_WAITING_LIST_REQUEST }
  | {
      type: WaitingListManagementAction.GET_WAITING_LIST_SUCCESS;
      payload: IWaitingListData;
    }
  | {
      type: WaitingListManagementAction.GET_WAITING_LIST_ERROR;
      payload: any;
    }
  | {
      type: WaitingListManagementAction.CREATE_NEW_ITEM;
      payload: IWaitingListEntry;
    }
  | {
      type: WaitingListManagementAction.DELETE_ITEM;
      payload: IWaitingListEntry;
    }
  | { type: WaitingListManagementAction.REORDER_LIST_DATA }
  | {
      type: WaitingListManagementAction.SEARCH_LIST_DATA;
      payload: ISearchList;
    }
  | {
      type: WaitingListManagementAction.FILTER_LIST_DATA;
      payload: ISearchList;
    };

export function fetchWaitingListRequest(): Action {
  return { type: WaitingListManagementAction.GET_WAITING_LIST_REQUEST };
}

export function fetchWaitingListSuccess(payload: IWaitingListData): Action {
  return {
    type: WaitingListManagementAction.GET_WAITING_LIST_SUCCESS,
    payload,
  };
}

export function fetchWaitingListError(payload: any): Action {
  return { type: WaitingListManagementAction.GET_WAITING_LIST_ERROR, payload };
}
export function createNewItem(payload: IWaitingListEntry): Action {
  return { type: WaitingListManagementAction.CREATE_NEW_ITEM, payload };
}
export const deleteItem = (payload: IWaitingListEntry): Action => {
  return { type: WaitingListManagementAction.DELETE_ITEM, payload };
};
export const reorderListData = (): Action => {
  return { type: WaitingListManagementAction.REORDER_LIST_DATA };
};
export const searchListData = (payload: ISearchList): Action => {
  return { type: WaitingListManagementAction.SEARCH_LIST_DATA, payload };
};
export const filterListData = (payload: ISearchList): Action => {
  return { type: WaitingListManagementAction.FILTER_LIST_DATA, payload };
};
