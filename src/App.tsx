// node_modules
import React, { lazy, Suspense, memo } from 'react';
import {
  HashRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
// components
import WaitingListManagementLayout from 'components/layouts/WaitingListManagementLayout';

const WaitingListManagementPage = lazy(
  () => import('pages/waitingListManagement/WaitingListManagementPage'),
);
const SettingPage = lazy(() => import('pages/settings/SettingPage'));

const App = () => (
  <>
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <WaitingListManagementLayout>
            <Switch>
              <Route
                exact
                path="/waiting-list-management"
                component={WaitingListManagementPage}
              />
              <Route exact path="/settings" component={SettingPage} />

              <Redirect to="/waiting-list-management" />
            </Switch>
          </WaitingListManagementLayout>
        </Switch>
      </Suspense>
    </Router>
  </>
);

export default memo(App);
