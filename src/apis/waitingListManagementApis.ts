import Dispatch from 'apis/client';

const urls = {
  getWaitingListData: (method: string) => {
    return {
      url: '',
      method,
    };
  },
  getWaitingListDataById: (method: string, id: string) => {
    return {
      url: `/data/${id}`,
      method,
    };
  },
};

function api() {
  return {
    getWaitingListData: (data) =>
      Dispatch(urls.getWaitingListData('get'), {}, data.payload),
    postWaitingListData: (data) =>
      Dispatch(urls.getWaitingListData('post'), {}, data.payload),
  };
}

export default api();
