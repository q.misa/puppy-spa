// actions
import {
  Action,
  WaitingListManagementAction,
} from 'actions/waitingListManagementActions';
import {
  IWaitingListData,
  IWaitingListEntry,
} from 'types/waitingListManagement';

export interface State {
  originalWaitingListData: IWaitingListData;
  waitingListData: IWaitingListData;
  waitingListItemFormValues: any;
  sortOrder: string;
}

export const initialState: State = {
  originalWaitingListData: { date: '', entries: [] },
  waitingListData: { date: '', entries: [] },
  waitingListItemFormValues: {
    serviced: false,
  },
  sortOrder: 'asc',
};

const updateWaitingListData = (data, values) => {
  const id = crypto.randomUUID();
  // const lastItemIndex = data.entries.findIndex((entry) => {
  //   return !entry.nextEntryId;
  // });

  const newItem = {
    ...values,
    prevEntryId: data.entries[data.entries.length - 1].id,
    nextEntryId: null,
    arrival: new Date(`${data.date} ${values.arrival}`),
    id,
  };

  data.entries[data.entries.length - 1].nextEntryId = id;

  return { ...data, entries: [...data.entries, newItem] };
};

const sortData = (
  data: IWaitingListEntry[],
  order: string,
): IWaitingListEntry[] => {
  return data.sort((a: IWaitingListEntry, b: IWaitingListEntry) =>
    order === 'asc'
      ? new Date(b.arrival).valueOf() - new Date(a.arrival).valueOf()
      : new Date(a.arrival).valueOf() - new Date(b.arrival).valueOf(),
  );
};

/**
 * try to achieve best practice with O(n)
 * @param items is waiting list object
 */
const reOrderData = (items: IWaitingListData) => {
  let pointer = undefined;
  const sorted = [];
  const mappedObject = items.entries.reduce((prev, curr: IWaitingListEntry) => {
    if (!curr.prevEntryId) {
      pointer = curr;
      sorted.push(curr);
    }
    return { ...prev, [curr.id]: curr };
  }, {});

  while (pointer) {
    if (!pointer.nextEntryId) break;
    const item = mappedObject[pointer.nextEntryId];
    sorted.push(item);
    pointer = item;
  }
  return sorted;
};

export const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case WaitingListManagementAction.GET_WAITING_LIST_REQUEST:
      return {
        ...state,
      };

    case WaitingListManagementAction.GET_WAITING_LIST_SUCCESS:
      const reOrderedData = reOrderData(action.payload);

      return {
        ...state,
        waitingListData: { ...action.payload, entries: reOrderedData },
        originalWaitingListData: { ...action.payload, entries: reOrderedData },
      };

    case WaitingListManagementAction.GET_WAITING_LIST_ERROR:
      return {
        ...state,
      };

    case WaitingListManagementAction.CREATE_NEW_ITEM:
      const data = updateWaitingListData(state.waitingListData, action.payload);
      return {
        ...state,
        waitingListData: data,
        originalWaitingListData: data,
      };

    case WaitingListManagementAction.DELETE_ITEM:
      const filterList = state.waitingListData.entries.filter(
        (entry: IWaitingListEntry) => entry.id !== action.payload.id,
      );

      return {
        ...state,
        waitingListData: {
          ...state.waitingListData,
          entries: filterList,
        },
        originalWaitingListData: {
          ...state.waitingListData,
          entries: filterList,
        },
      };

    case WaitingListManagementAction.REORDER_LIST_DATA:
      const sortedData = sortData(
        state.waitingListData.entries,
        state.sortOrder,
      );
      return {
        ...state,
        waitingListData: { ...state.waitingListData, entries: sortedData },
        sortOrder: state.sortOrder === 'asc' ? 'desc' : 'asc',
      };

    case WaitingListManagementAction.SEARCH_LIST_DATA:
      const { searchKey, value } = action.payload;
      const searchItem = state.waitingListData.entries.filter((entry) =>
        entry[searchKey].toLowerCase().includes(value),
      );
      return {
        ...state,
        waitingListData: { ...state.waitingListData, entries: searchItem },
      };

    case WaitingListManagementAction.FILTER_LIST_DATA:
      const filterItem = state.originalWaitingListData.entries.filter(
        (entry: IWaitingListEntry) =>
          entry[action.payload.searchKey] === action.payload.value,
      );
      return {
        ...state,
        waitingListData: { ...state.waitingListData, entries: filterItem },
      };

    default: {
      return state;
    }
  }
};
