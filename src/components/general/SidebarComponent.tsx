// node modules
import * as React from 'react';
import { Layout, Drawer } from 'antd';
// components
import SidebarMenuComponent from 'components/general/SidebarMenuComponent';
// images
import puppy_spa from 'assets/images/puppy_spa.jpg';
import { PUPPY_SPA } from 'constants/texts';
// styles
import 'styles/sidebar.css';

const { Sider } = Layout;

const SidebarComponent = () => {
  const [visibleDrawer, setVisibleDrawer] = React.useState<boolean>(false);

  const handleCollapse = React.useCallback(
    (collapse: boolean, type: string) => {
      if (type === 'clickTrigger' && !collapse) {
        setVisibleDrawer(true);
      } else if (type === 'responsive' && collapse) {
        setVisibleDrawer(false);
      }
    },
    [],
  );

  const onCloseDrawer = React.useCallback(() => {
    setVisibleDrawer(false);
  }, []);

  return (
    <div data-testid="sidebar">
      {visibleDrawer ? (
        <Drawer
          className="text-align-center"
          title={
            <>
              <h2>{PUPPY_SPA}</h2>
              <img
                className="margin-auto puppy-spa-logo-sidebar"
                alt="puppy"
                src={puppy_spa}
              />
            </>
          }
          placement="left"
          closable={true}
          onClose={onCloseDrawer}
          open={visibleDrawer}
        >
          <SidebarMenuComponent />
        </Drawer>
      ) : (
        <Sider
          className="sidebar-wrapper text-align-center"
          width={272}
          breakpoint="md"
          collapsedWidth={0}
          theme={'light'}
          onCollapse={handleCollapse}
        >
          <h2>{PUPPY_SPA}</h2>
          <img
            className="margin-auto puppy-spa-logo-sidebar"
            alt="puppy"
            src={puppy_spa}
          />
          <SidebarMenuComponent />
        </Sider>
      )}
    </div>
  );
};

export default SidebarComponent;
