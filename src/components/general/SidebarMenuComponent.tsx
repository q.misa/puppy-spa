// node modules
import * as React from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { Menu, Row, Col } from 'antd';
import { UnorderedListOutlined, SettingOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
// texts
import { WAITING_LIST, SETTINGS } from 'constants/texts';

type MenuItem = Required<MenuProps>['items'][number];
function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const SidebarMenuComponent = () => {
  const history = useHistory();
  const pathName = history.location.pathname;

  const returnIconClass = React.useCallback(
    (path: string) => {
      return pathName === path
        ? 'active-sidebar-icon-waiting-list-management'
        : 'waiting-list-management';
    },
    [pathName],
  );
  const items: MenuItem[] = [
    getItem(
      <div className="sidebar-menu misa">
        <NavLink
          exact
          to="/waiting-list-management"
          activeClassName="panel-active-menu-item"
        >
          <Row>
            <Col offset={1} className="sidebar-icon-common">
              <UnorderedListOutlined
                className={returnIconClass('/waiting-list-management')}
              />
            </Col>
            <Col offset={2}>
              <span
                className={`font-weight-bold ${
                  pathName === '/waiting-list-management'
                    ? 'sidebar-active-color'
                    : 'sidebar-color'
                }
                      `}
              >
                {WAITING_LIST}
              </span>
            </Col>
          </Row>
        </NavLink>
        ,
      </div>,
      'waiting-list-management',
    ),
    getItem(
      <div className="sidebar-menu">
        <NavLink exact to="settings" activeClassName="panel-active-menu-item">
          <Row>
            <Col offset={1} className="sidebar-icon-common">
              <SettingOutlined className={returnIconClass('/settings')} />
            </Col>
            <Col offset={2}>
              <span
                className={`font-weight-bold ${
                  pathName === '/settings'
                    ? 'sidebar-active-color'
                    : 'sidebar-color'
                }`}
              >
                {SETTINGS}
              </span>
            </Col>
          </Row>
        </NavLink>
        ,
      </div>,
      'settings',
    ),
  ];

  return (
    <Menu
      mode="inline"
      selectedKeys={[history.location.pathname]}
      items={items}
    />
  );
};

export default SidebarMenuComponent;
