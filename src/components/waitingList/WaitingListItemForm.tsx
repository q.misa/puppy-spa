// node modules
import * as React from 'react';
import { Form, Input, TimePicker, Switch } from 'antd';
// texts
import {
  PUPPY_NAME,
  OWNER_NAME,
  REQUESTED_SERVICE,
  THIS_FIELD_IS_REQUIRED,
  SERVICED,
  ARRIVAL,
  PLZ_SELECT_TIME,
} from 'constants/texts';

const config = {
  rules: [
    { type: 'object' as const, required: true, message: PLZ_SELECT_TIME },
  ],
};

const WaitingListItemForm = (props: { mode: string; serviced?: boolean }) => {
  return (
    <div data-testid="waiting-list-item-form">
      <Form.Item
        label={PUPPY_NAME}
        name="puppyName"
        rules={[
          {
            required: true,
            message: THIS_FIELD_IS_REQUIRED,
          },
        ]}
      >
        <Input
          data-type="puppyName"
          size="large"
          bordered={false}
          placeholder={PUPPY_NAME}
          className="waiting-list-form-item"
          disabled={props.mode === 'edit'}
        />
      </Form.Item>
      <Form.Item
        label={OWNER_NAME}
        name="owner"
        rules={[
          {
            required: true,
            message: THIS_FIELD_IS_REQUIRED,
          },
        ]}
      >
        <Input
          data-type="owner"
          size="large"
          bordered={false}
          placeholder={OWNER_NAME}
          className="waiting-list-form-item"
          disabled={props.mode === 'edit'}
        />
      </Form.Item>
      <Form.Item
        label={REQUESTED_SERVICE}
        name="requestedService"
        rules={[
          {
            required: true,
            message: THIS_FIELD_IS_REQUIRED,
          },
        ]}
      >
        <Input
          data-type="requestedService"
          size="large"
          bordered={false}
          placeholder={REQUESTED_SERVICE}
          className="waiting-list-form-item"
          disabled={props.mode === 'edit'}
        />
      </Form.Item>
      <Form.Item name="arrival" label={ARRIVAL} {...config}>
        {props.mode === 'edit' ? (
          <Input
            data-type="arrival"
            size="large"
            bordered={false}
            placeholder={ARRIVAL}
            className="waiting-list-form-item"
            disabled
          />
        ) : (
          <TimePicker format="HH:mm:ss" className="waiting-list-form-item" />
        )}
      </Form.Item>
      <Form.Item name="serviced" label={SERVICED}>
        <Switch checked={props.serviced} disabled={props.mode === 'edit'} />
      </Form.Item>
    </div>
  );
};

export default WaitingListItemForm;
