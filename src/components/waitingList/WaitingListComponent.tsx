// node modules
import * as React from 'react';
import { List, Avatar, Typography, Modal } from 'antd';
import { EyeOutlined, DeleteOutlined } from '@ant-design/icons';
// components
import WaitingListItemForm from './WaitingListItemForm';
import WaitingListDescription from './WaitingListDescription';
import ModalForm from 'components/general/ModalForm';
// hooks
import useModal from 'hooks/useModal';
// texts
import {
  ITEMS,
  VIEW_ITEM,
  DELETE_RECORD_CONFIRMATION_MESSAGE,
  DELETE,
} from 'constants/texts';
import {
  IWaitingListData,
  IWaitingListEntry,
} from 'types/waitingListManagement';
import 'styles/confirmationModal.css';

const { confirm } = Modal;
const { Text, Title } = Typography;

type Props = {
  waitingListData: IWaitingListData;
  deleteItem: (selectedItem: IWaitingListEntry) => void;
};

const WaitingListComponent = (props: Props) => {
  /**************************************************** STATES ******************************************************/
  const [selectedData, setSelectedData] = React.useState(null);
  const { isShowing, toggle } = useModal();

  /************************************************* FUNCTIONS ******************************************************/
  const getSelectedData = React.useCallback(
    (data: IWaitingListEntry) => {
      setSelectedData(data);
      toggle('edit-item');
    },
    [toggle],
  );

  const openModalDeleteAction = (record: IWaitingListEntry) => {
    confirm({
      width: 500,
      className: 'text-align-center confirmation-modal-container',
      cancelButtonProps: { style: { display: 'none' } },
      closable: true,
      icon: <></>,
      okText: DELETE,
      title: <Title level={4}>{DELETE}</Title>,
      content: (
        <Text className="delete-modal">{`${DELETE_RECORD_CONFIRMATION_MESSAGE} ( ${record.puppyName} )`}</Text>
      ),
      onOk() {
        props.deleteItem(record);
      },
    });
  };

  return (
    <div data-testid="waiting-list-component" className="waiting-list-wrapper">
      <List
        header={
          <Typography.Title level={3}>
            {props.waitingListData.entries.length} {ITEMS} in:{' '}
            {props.waitingListData.date}
          </Typography.Title>
        }
        className="padding-2"
        loading={!props.waitingListData.entries.length}
        itemLayout="horizontal"
        dataSource={props.waitingListData.entries}
        renderItem={(item: IWaitingListEntry) => (
          <List.Item
            actions={[
              <EyeOutlined
                key="view-item"
                title="view"
                data-testid={item.id}
                className="cursor-pointer"
                onClick={() => getSelectedData(item)}
              />,
              <DeleteOutlined
                key="delete-item"
                title="delete"
                className="cursor-pointer"
                onClick={() => openModalDeleteAction(item)}
              />,
            ]}
          >
            <List.Item.Meta
              avatar={
                <Avatar src="https://as2.ftcdn.net/v2/jpg/01/43/31/65/1000_F_143316541_R9OHDwC1JbIUmvXlhs2PY79jnlxIWTho.jpg" />
              }
              title={item.puppyName}
              description={<WaitingListDescription {...item} />}
            />
          </List.Item>
        )}
      />
      <ModalForm
        initialValues={selectedData}
        title={VIEW_ITEM}
        size="large"
        width={700}
        visible={isShowing === 'edit-item'}
        dataType="waiting-list-form"
        onCancel={toggle}
        footer={null}
      >
        <WaitingListItemForm mode="edit" serviced={selectedData?.serviced} />
      </ModalForm>
    </div>
  );
};

export default WaitingListComponent;
