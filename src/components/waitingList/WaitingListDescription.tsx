import * as React from 'react';
import Text from 'antd/lib/typography/Text';
import { ARRIVAL, REQUESTED_SERVICE, SERVICED } from 'constants/texts';

type Props = {
  id: string;
  owner: string;
  arrival: string | Date | number;
  requestedService: string;
  serviced: boolean;
};
const WaitingListDescription = ({
  id,
  owner,
  arrival,
  requestedService,
  serviced,
}: Props) => {
  // ************************************************* FUNCTIONS ******************************************************

  const makeDetails = () => {
    return (
      <div className="display-flex justify-content-space-around description-wrapper">
        <div>{`${ARRIVAL} : ${new Date(arrival).toLocaleTimeString()}`}</div>
        <div>
          {REQUESTED_SERVICE}: {requestedService}
        </div>
        <div>{`${SERVICED}: ${serviced}`}</div>
      </div>
    );
  };

  return (
    <div data-testid="waiting-list-description" key={id}>
      <div>
        {makeDetails()}
        <br />
        <Text italic strong mark>
          {`Owner is : ${owner}`}
        </Text>
      </div>
    </div>
  );
};

export default WaitingListDescription;
