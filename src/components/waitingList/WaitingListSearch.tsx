import * as React from 'react';
import { Select, Input } from 'antd';
import {
  PUPPY_NAME,
  PUPPY,
  SEARCH,
  ENTER_SEARCH_VALUE,
  OWNER_NAME,
} from 'constants/texts';
import 'styles/search.css';

const { Option, OptGroup } = Select;
const { Search } = Input;

type Props = {
  search: ({ searchKey, value }) => void;
};

const WaitingListSearch = ({ search }: Props) => {
  const [searchKey, setSearchKey] = React.useState<string>('puppyName');

  const handleChangeSearchKey = React.useCallback((key: string) => {
    setSearchKey(key);
  }, []);

  const handleOnSearch = React.useCallback(
    (value?: string) => {
      search({ searchKey, value });
    },
    [searchKey, search],
  );

  return (
    <div className="text-align-center search-container display-flex justify-content-center">
      <Select
        size="large"
        defaultValue="puppyName"
        style={{ width: 200 }}
        onChange={handleChangeSearchKey}
      >
        <OptGroup label={PUPPY}>
          <Option value="puppyName">{PUPPY_NAME}</Option>
          <Option value="owner">{OWNER_NAME}</Option>
        </OptGroup>
      </Select>
      <div className="search-btn-wrapper">
        <Search
          placeholder={ENTER_SEARCH_VALUE}
          enterButton={SEARCH}
          size="large"
          allowClear
          loading={false}
          onSearch={handleOnSearch}
        />
      </div>
    </div>
  );
};
export default WaitingListSearch;
