import * as React from 'react';
import { Button, Checkbox } from 'antd';
import {
  ClearOutlined,
  PlusCircleOutlined,
  SortDescendingOutlined,
} from '@ant-design/icons';
import WaitingListSearch from './WaitingListSearch';
import { NEW, REORDER_LIST, SERVICED, RESET_FILTERS } from 'constants/texts';
import 'styles/search.css';

const WaitingListActions = ({
  reorderList,
  search,
  resetFilters,
  handleOnChange,
  showModal,
}) => {
  return (
    <div className="display-flex justify-content-space-between action-wrapper">
      <Button
        className="margin-left-2 margin-right-2 margin-top-1"
        type="primary"
        shape="round"
        icon={<PlusCircleOutlined />}
        size="large"
        onClick={showModal}
      >
        {NEW}
      </Button>
      <Button
        className="margin-left-1 margin-right-1 margin-top-1 align-self-center"
        type="primary"
        shape="round"
        icon={<SortDescendingOutlined />}
        size="small"
        onClick={reorderList}
      >
        {REORDER_LIST}
      </Button>
      <Button
        className="margin-left-1 margin-right-1 margin-top-1 align-self-center"
        type="primary"
        shape="round"
        icon={
          <Checkbox
            className="checkbox-serviced"
            onChange={handleOnChange}
          ></Checkbox>
        }
        size="small"
      >
        {SERVICED}
      </Button>
      <Button
        className="margin-left-2 margin-right-2 margin-top-1 align-self-center"
        type="primary"
        shape="round"
        icon={<ClearOutlined />}
        size="middle"
        onClick={resetFilters}
      >
        {RESET_FILTERS}
      </Button>
      <WaitingListSearch search={search} />
    </div>
  );
};
export default WaitingListActions;
